package horseracing

fun main(args : Array<String>) {
    val powers = (0 until readLine()!!.toInt()).map{readLine()!!.toInt()}.sorted()
    println((1 until powers.size).map{powers[it]-powers[it-1]}.min())
}