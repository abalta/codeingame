package horseracing


fun main(args : Array<String>) {
    val N = readLine()!!.toInt()
    var index = 0
    val powers = (0 until N).map { readLine()!!.toInt() }.sorted()
    val differences = powers.map { it ->
        index += 1
        val d: Int = if(index < powers.size)
            powers[index] - it
        else
            it * it
        d
    }.min()
    println(differences)
}