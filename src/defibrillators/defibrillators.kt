package defibrillators

fun main(args : Array<String>) {
    val LON = readLine()!!.replaceAndDouble()
    val LAT = readLine()!!.replaceAndDouble()
    val N = readLine()!!.toInt()
    val defibs = (0 until N).map { readLine()!! }
    val closestDefib = defibs.map { it ->
            val defibInfo = it.split(';')
            val lat = defibInfo.last().replaceAndDouble()
            val long = defibInfo[defibInfo.lastIndex - 1].replaceAndDouble()
        distance(LON, LAT, long, lat)
    }

    val minIdx = closestDefib.indexOf(closestDefib.min())
    println(defibs[minIdx].split(';')[1])



    // Write an action using println()
    // To debug: System.err.println("Debug messages...");
}

fun String.replaceAndDouble() = replace(',','.').toDouble()
fun Double.pow() = this * this


fun distance(lonU: Double, latU: Double, lonD: Double, latD: Double): Double{
    val x = (lonD - lonU) * Math.cos((latD + latU)/2)
    val y = latD - latU
    return Math.sqrt(x.pow() + y.pow()) * 6371
}
