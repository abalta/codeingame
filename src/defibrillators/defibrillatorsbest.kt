package defibrillators

import java.util.*
import java.io.*
import java.math.*

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
fun main(args : Array<String>) {

    fun toRad(degree: String) = degree.replace(',', '.').toDouble() * Math.PI / 180f

    val LON = toRad(readLine()!!.toString())
    val LAT = toRad(readLine()!!.toString())
    val N = readLine()!!.toInt()

    val closest = (0 until N).map {
        val defib = readLine()!!.toString().split(';')
        val lon = toRad(defib[4])
        val lat = toRad(defib[5])
        val x = (LON - lon) * Math.cos((lat + LAT) /2)
        val y = LAT - lat
        val d = Math.sqrt(x * x + y * y ) * 6371
        Pair(defib[1], d)
    }.minBy {it.second}

    println("${closest!!.first}")

}