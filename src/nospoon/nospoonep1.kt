package nospoon

fun main(args : Array<String>) {

    val width = readLine()!!.toInt()
    val height = readLine()!!.toInt()
    val rows = (0 until height).map { readLine()!!.toCharArray() }
    rows.forEachIndexed { index, chars ->
        for (i in 0 until width){
            var x2 = -1
            var x3 = -1
            var y2 = -1
            var y3 = -1
            if(chars[i].isDigit()){
                //Found node
                val x1 = i
                val y1 = index
                //Looking for right neighbor
                if(i + 1 < width){
                    for(j in i+1 until width){
                        if(chars[j].isDigit()){
                            x2 = j
                            y2 = index
                            break
                        }
                    }
                }
                //Looking for bottom neighbor
                if(index + 1 < height){
                    for(j in index+1 until height){
                        if(rows[j][i].isDigit()){
                            x3 = i
                            y3 = j
                            break
                        }
                    }
                }
                println("$x1 $y1 $x2 $y2 $x3 $y3")
            }
        }
    }
}