package nospoon

import java.util.*

/**
 * Don't let the machines win. You are humanity's last hope...
 **/
fun main(args : Array<String>) {
    val input = Scanner(System.`in`)
    val width = input.nextInt() // the number of cells on the X axis
    val height = input.nextInt() // the number of cells on the Y axis
    if (input.hasNextLine()) {
        input.nextLine()
    }
    val grid = (0 until height).map {
        // width characters, each either 0 or .
        input.nextLine().map { it == '0' }
    }

    for (x in 0 until width)
        for (y in 0 until height) {
            if (grid[y][x]) {
                print("$x $y ")
                val right = (x+1 until width).firstOrNull { grid[y][it] }
                if (right == null)
                    print("-1 -1 ")
                else
                    print("${right} $y ")

                val down = (y+1 until height).firstOrNull { grid[it][x] }
                if (down == null)
                    print("-1 -1 ")
                else
                    print("${x} ${down} ")
                println()
            }
        }
}