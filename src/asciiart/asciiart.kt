import java.util.*

fun main(args : Array<String>) {
    val asciiArts = "ABCDEFGHIJKLMNOPQRSTUVWXYZ?"
    val input = Scanner(System.`in`)
    val L = input.nextInt()
    val H = input.nextInt()
    if (input.hasNextLine()) {
        input.nextLine()
    }
    val T = input.nextLine()
    val rowList = mutableMapOf<Int, List<String>>()
    for (i in 0 until H) {
        val ROW = input.nextLine()
        rowList.put(i, ROW.chunkedExt(L))
    }

    for (j in 0 until H) {
        for (i in 0 until T.length){
            if(T.toUpperCase().get(i).isLetter()){
                val rowKey = asciiArts.indexOf(T.toUpperCase().get(i))
                print(rowList.get(j)?.get(rowKey))
            }else{
                print(rowList.get(j)?.get(asciiArts.indexOf('?')))
            }
        }
        println()
    }
}

fun String.chunkedExt(size: Int): List<String> {
    val numChunks = Math.ceil((length / size).toDouble())
    val chunks = mutableListOf<String>()
    var offset = 0
    for (i in 0..numChunks.toInt()) {
        offset = i * size
        if(offset + size > length)
            chunks.add(i, substring(offset, length))
        else
            chunks.add(i, substring(offset, offset + size))
    }

    return chunks
}
 