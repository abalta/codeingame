package asciiart

fun main(args : Array<String>) {
    val L = readLine()!!.toInt()
    val H = readLine()!!.toInt()
    val T = readLine()!!.toUpperCase().replace("[^A-Z]".toRegex(), "[")

    val rows = (0 until H).map{readLine()!!}

    (0 until H).map{h -> T.fold(""){ a, c -> a + rows[h].substring((c-'A')*L, (c-'A')*L+L) }}.forEach{println(it)}
}