package chucknorris

fun Char.toEncodedString(count: Int) = (if(this == '1') "0 " else "00 ") + "0".repeat(count)

fun String.countCharWhile(symbol: Char) = takeWhile { it == symbol}.count()

fun String.encode(): String {
    var list = ArrayList<String>()

    var i=0
    while(i < this.length) {
        val text = substring(i)
        var count = text.countCharWhile(this[i])

        list.add(this[i].toEncodedString(count))
        i += count
    }

    return list.joinToString(" ")
}

fun String.toBinaryString() = toCharArray()
        .map { it.toInt().toString(2).padStart(7, '0') }
        .joinToString("")

fun main(args : Array<String>) = println(readLine()!!.toBinaryString().encode())