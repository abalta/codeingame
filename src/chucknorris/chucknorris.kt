package chucknorris

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
fun main(args : Array<String>) {
    //val c = "CC".toCharArray()
    val c = readLine()!!
    val ascii = convertCharToAscii(c.toCharArray())
    val binary = convertDecimalToBinary(ascii)
    println(convertAsciiToChuckNorris(groupAsciiToChuckNorris(binary)))
}

fun convertAsciiToChuckNorris(asciiList: List<String>): String{
    val result = StringBuilder()
    val zero = "0"
    for(i in 0 until asciiList.size){
        if(asciiList.get(i).contains('1'))
            result.append("0 ${zero.repeat(asciiList.get(i).length)} ")
        else
            result.append("00 ${zero.repeat(asciiList.get(i).length)} ")
    }
    return result.toString().trim()
}

fun groupAsciiToChuckNorris(ascii: String): List<String> {
    var changed = false
    var result = StringBuilder()
    for(i in 0 until ascii.length){
        if(!changed){
            result.append(ascii[i])
        }else{
            result.append(' ')
            result.append(ascii[i])
        }
        if(i < ascii.length - 1){
            changed = ascii[i] != ascii[i + 1]
        }
    }
    return result.toString().split(' ')
}

fun convertCharToAscii(chars: CharArray): IntArray {
    val asciiArray = IntArray(chars.size)
    var i = 0
    for(char in chars){
        val ascii = char.toInt()
        asciiArray.set(i++, ascii)
    }
    return asciiArray
}

fun convertDecimalToBinary(n: IntArray): String {
    val ascii = n
    val sbResult = StringBuilder()
    for(index in 0 until n.size){
        var binaryNumber: Long = 0
        var remainder: Int
        var i = 1
        while (ascii[index] != 0) {
            remainder = ascii[index] % 2
            ascii[index] /= 2
            binaryNumber += (remainder * i).toLong()
            i *= 10
        }
        val length = binaryNumber.toString().length
        if(length < 7){
            val zero = "0"
            sbResult.append("${zero.repeat(7 - length)}${binaryNumber.toString()}")
        }else
            sbResult.append(binaryNumber.toString())
    }
    return sbResult.toString()
}