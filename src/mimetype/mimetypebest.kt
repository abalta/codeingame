package mimetype

import java.io.*

fun main(args : Array<String>) {
    val N = readLine()!!.toInt()
    val Q = readLine()!!.toInt()

    val mimeTypes = (0 until N).associate {
        val (extension, mimeType) = readLine()!!.split(' ')
        extension.toLowerCase() to mimeType
    }

    val filenames = (0 until Q).map { readLine()!! }

    val extensions = filenames.map { filename ->
        val extension = File(filename).extension.toLowerCase()
        mimeTypes[extension] ?: "UNKNOWN"
    }

    extensions.forEach(::println)
}