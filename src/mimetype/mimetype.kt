package mimetype

import java.util.*
import kotlin.collections.HashMap

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
fun main(args : Array<String>) {
    val mimeTypeMap = HashMap<String, String>()
    val input = Scanner(System.`in`)
    val N = input.nextInt() // Number of elements which make up the association table.
    val Q = input.nextInt() // Number Q of file names to be analyzed.
    for (i in 0 until N) {
        val EXT = input.next() // file extension
        val MT = input.next() // MIME type.
        mimeTypeMap.put(EXT.toLowerCase(), MT)
    }
    input.nextLine()
    for (i in 0 until Q) {
        val FNAME = input.nextLine() // One file name per line.
        val fnameAry = FNAME.split(".")
        val key = fnameAry[fnameAry.size - 1].toLowerCase()
        if(mimeTypeMap.containsKey(key) && fnameAry.size > 1)
            println(mimeTypeMap[key])
        else
            println("UNKNOWN")
    }

    // Write an action using println()
    // To debug: System.err.println("Debug messages...");


    // For each of the Q filenames, display on a line the corresponding MIME type. If there is no corresponding type, then display UNKNOWN.
}